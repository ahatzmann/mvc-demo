<?php

// Initialize site configuration
require_once('includes/config.inc.php');

// Check the querystring for a numeric id
if (isset($_GET['id']) && intval($_GET['id']) > 0) {

	// Initialize form values
	$title = NULL;
	$content = NULL;

	// Get id from querystring
	$id = $_GET['id'];
		
	// Check for inital page request
	if ($_SERVER['REQUEST_METHOD'] == 'GET') {

		// Execute database query
		$row = get_post_by_id($id);
							
		// Set form values
		$title = $row['title'];
		$content = $row['content'];
	} 
	
	// Check for page postback
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				
		// Get user input from form
		$title = $_POST['title'];
		$content = $_POST['content'];

		// Execute database query
		update_post($title, $content, $id);
			
		// Redirect to site root
		redirect_to('.');	
	} 

} else {

	// Redirect to site root
	redirect_to('.');	
}

?>

<?php require_once(TEMPLATE_PATH.'header.inc.php'); ?>

	<form method="POST" action="<?php 
		echo sanitize_output($_SERVER['REQUEST_URI']);?>">

		<p>						
			<label for="title">Title</label><br />									
			<input id="title" name="title" type="text" size="75" value="<?php 
				echo sanitize_output($title); ?>" autofocus/></p>

		<p>
			<label for="content">Content</label><br />
			<textarea id="content" name="content"><?php 
				echo sanitize_output($content);?></textarea></p>
		
		<p>
			<input type="submit"/></p>

	</form>

<?php require_once(TEMPLATE_PATH.'footer.inc.php'); ?>