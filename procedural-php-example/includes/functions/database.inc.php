<?php

function open_database() {

	// Set global variable scope
	global $connection;

	// Open database connection
	$connection = mysql_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD) 
		or die(mysql_error());
	
	// Select target database
	mysql_select_db(DATABASE_NAME, $connection) 
		or die(mysql_error());
}

function execute_statement($statement) {

	// Get connection from global scope
	global $connection;
	
	// Open database connection
	open_database();
	
	// Execute database query
	$result = mysql_query($statement, $connection) 
		or die(mysql_error());
	
	// Close database connection
	close_database();
	
	// Return results
	return $result;
}

function execute_non_query($dml) {

	// Execute database query
	$result = execute_statement($dml);

	// Get affected rows
	return mysql_affected_rows($result);
}

function execute_query($sql) {
	
	// Initialize dataset
	$dataset = FALSE;
	
	// Execute database query
	$result = execute_statement($sql);
	
	// Count number of rows
	$count = mysql_num_rows($result);

	// Check for single row returned
	if ($count == 1) {
	
		// Fetch a single row from database cursor
		$dataset = mysql_fetch_assoc($result);	
	}
	
	// Check for multiple rows returned
	if ($count > 1) {

		// Initialize rows array
		$dataset = array();
		
		// Fetch all rows from database cursor
		while ($row = mysql_fetch_assoc($result)) {
			$dataset[] = $row;
		}
	}	
	
	// Return dataset
	return $dataset;
}

function close_database() {

	// Set global variable scope
	global $connection;
	
	// Close database connection
	if (isset($connection)) {
		mysql_close($connection);
	}
}