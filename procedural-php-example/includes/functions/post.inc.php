<?php

function get_posts() {

	// Build database query
	$sql = 'select * from post';
	
	// Execute query and return all rows
	return execute_query($sql);
}

function get_post_by_id($id) {

	// Sanitize user input
	$id = (int)$id;
	
	// Build database query
	$sql = sprintf("select * from post where post_id = %d limit 1", $id);

	// Execute query and return single row
	return execute_query($sql);
}

function insert_post($title, $content) {

	// Sanitize user input
	$title = sanitize_input($title);
	$content = sanitize_input($content);

	// Build database query
	$sql = sprintf("insert into post (title, content, created) values ('%s', '%s', NOW())", $title, $content);
	
	// Execute non-query and return rows affected
	return execute_non_query($sql);
}

function update_post($title, $content, $id) {

	// Sanitize user input
	$id = (int)$id;
	$title = sanitize_input($title);
	$content = sanitize_input($content);

	// Build database query	
	$sql = sprintf("update post set title = '%s', content = '%s' where post_id = %d", $title, $content, $id);

	// Execute non-query and return rows affected
	return execute_non_query($sql);	
}

function delete_post($id) {

	// Sanitize user input
	$id = (int)$id;
	
	// Build database query
	$sql = sprintf("delete from post where post_id = %d limit 1", $id);

	// Execute non-query and return rows affected
	return execute_non_query($sql);	
}