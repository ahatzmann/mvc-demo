<?php

// Initialize site configuration
require_once('includes/config.inc.php');

// Initialize form values
$title = NULL;
$content = NULL;

// Check for page postback
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	// Get user input from form
	$title = $_POST['title'];
	$content = $_POST['content'];

	// Execute database query
	insert_post($title, $content);
	
	// Redirect to site root
	redirect_to('.');
}

?>

<?php require_once(TEMPLATE_PATH.'header.inc.php'); ?>

	<form method="POST" action="<?php 
		echo sanitize_output($_SERVER['PHP_SELF']); ?>">
	
		<p>	
			<label for="title">Title</label><br />
			<input id="title" name="title" type="text" size="75" value="<?php 
				echo sanitize_output($title);?>" autofocus/></p>

		<p>
			<label for="content">Content</label><br />
			<textarea id="content" name="content"><?php 
				echo sanitize_output($content);?></textarea></p>
	
		<p>
			<input type="submit"/></p>
		
	</form>

<?php require_once(TEMPLATE_PATH.'footer.inc.php'); ?>