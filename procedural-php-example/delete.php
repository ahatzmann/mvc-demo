<?php

// Initialize site configuration
require_once('includes/config.inc.php');

// Check the querystring for a numeric id
if (isset($_GET['id']) && intval($_GET['id']) > 0) {
	
	// Execute database query
	delete_post($_GET['id']);
}

// Redirect to site root
redirect_to('.');