<?php 

// Initialize site configuration
require_once('includes/config.inc.php');

// Get rows from database
$posts = get_posts();

?>

<?php require_once(TEMPLATE_PATH.'header.inc.php');?>

	<?php foreach($posts as $post): ?>

		<h4>
			<a href="read.php?id=<?php 
				echo $post['post_id'];?>"><?php 
				echo $post['title'];?>
			</a>
		</h4>

		<p>
			<?php echo $post['content'];?>
			<?php echo $post['created'];?>
		</p>

	<?php endforeach;?>

<?php require_once(TEMPLATE_PATH.'footer.inc.php');?>