<?php

namespace Controller;

use Model\Post;

class HomeController extends Controller {

	public function index() {
		//Declare variables here!
		$posts = array();

		for($i = 1; $i < 13; $i++)
			$posts[] = new Post($i);

		//This is how you call a view! Mind that the ->with() part is optional, but necessary when passing variables to a view!
		return $this->view('home')->with(['posts' => $posts]);
	}

}

?>