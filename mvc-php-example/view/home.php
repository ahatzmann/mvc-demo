<?php

$posts = $variables['posts'];

?>

<html>

<head>
	<title>Home</title>
</head>

<body>
	<?php
	foreach ($posts as $post) {
	?>
	<div>
		<h2>
			<?php
			
			//This is how you use variables from the Controller.
			echo $post->get('title');
			
			?>
		</h2>
		<p>
			<?php

			echo $post->get('content');

			?>
		</p>
		<p>
			Created on: 
			<?php

			echo $post->get('created');

			?>
		</p>
	</div>
	<?php
	}
	?>
</body>

</html>
