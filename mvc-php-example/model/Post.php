<?php

namespace Model;

class Post extends Model {
	private $id;

	/**
	 * Fetches the post object from the database.
	 * @param int $id
	 */
	public function __construct($id) {
		$this->id = $id;
		$this->load();
	}

	public function load() {
		$sql = "select * from post where post_id = " . $this->id . ";";

		$result = \DatabaseHandler::execute_query($sql);

		foreach($result as $key => $value) {
			$this->set($key, $value);
		}
	}

	public function save() {
		$variables = $this->getVariables();

		$sql = "update post set title = \'" . $this->get('title') . "\', content = \'" . $this->get('content') . "\', created = \'" . $this->get('created') . "\' where post_id = " . $this->id . ";";
	}
}

?>