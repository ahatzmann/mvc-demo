<?php

spl_autoload_register(function ($class) {
	//Remove any '\' and preceeding namespace mentions.
	if (strrpos($class, "\\") != null)
		$class = substr($class, strrpos($class, "\\") + 1);

	$file = "{$class}.php";

	if (file_exists($file))
		include $file;
	else if (file_exists("model/" . $file))
		include "model/" . $file;
	else if (file_exists("controller/" . $file))
		include "controller/" . $file;
});

?>