-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2016 at 12:12 
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


--
-- Database: `mvc_workshop`
--
CREATE DATABASE IF NOT EXISTS `mvc_workshop` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mvc_workshop`;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `title`, `content`, `created`) VALUES
(1, 'Test 1', 'Test test test test test test test test.', '2016-05-23 12:43:12'),
(2, 'Test 2', 'Test test test test test test test test.', '2016-05-23 12:44:12'),
(3, 'Test 3', 'Test test test test test test test test.', '2016-05-23 12:45:12'),
(4, 'Test 4', 'Test test test test test test test test.', '2016-05-23 12:46:12'),
(5, 'Test 5', 'Test test test test test test test test.', '2016-05-23 12:47:12'),
(6, 'Test 6', 'Test test test test test test test test.', '2016-05-23 12:48:12'),
(7, 'Test 7', 'Test test test test test test test test.', '2016-05-23 12:49:12'),
(8, 'Test 8', 'Test test test test test test test test.', '2016-05-23 12:50:12'),
(9, 'Test 9', 'Test test test test test test test test.', '2016-05-23 12:51:12'),
(10, 'Test 10', 'Test test test test test test test test.', '2016-05-23 12:52:12'),
(11, 'Test 11', 'Test test test test test test test test.', '2016-05-23 12:53:12'),
(12, 'Test 12', 'Test test test test test test test test.', '2016-05-23 12:54:12'),
(14, 'Test 13', 'Test test test test test test test test.', '2016-05-24 12:50:31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
