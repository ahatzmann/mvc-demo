<?php

namespace Model;

class View {
	private $view = null;
	private $variables = null;

	public function __construct($url) {
		$this->view = $url;
	}

	public function with(array $variables) {
		$this->variables = $variables;
		return $this;
	}

	public function get($variable_name) {
		return $this->$variable_name;
	}

}

?>