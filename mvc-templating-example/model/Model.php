<?php
namespace Model;

abstract class Model {
	private $variables = array();

	public abstract function load();

	public abstract function save();

	public function get($key) {
		return $this->variables[$key];
	}

	public function set($key, $value) {
		$this->variables[$key] = $value;
	}

	public function getVariables() {
		return $this->variables;
	}
}

?>