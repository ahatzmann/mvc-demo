<?php

require_once("autoload.php");

$page = "Home";
$action = "index";

if (isset($_GET['page']) && !empty($_GET['page']))
	$page = ucfirst($_GET['page']);

if (isset($_GET['action']) && !empty($_GET['action']))
	$action = $_GET['action'];

$controller = $page . "Controller";

$view = null;

if (file_exists("controller/" . $controller . ".php")) {
	include "controller/" . $controller . ".php";
	$controller = "Controller\\" . $controller;
	$object = new $controller;

	if(!function_exists($action) && !is_callable($action)) {
		$action = "index";
	}

	$view_object = $object->$action();
	$view = $view_object->get('view');
	$variables = $view_object->get('variables');
    $smarty = new Smarty();
	$smarty->setTemplateDir("./view");
	$smarty->assign($variables);
    $smarty->display($view);
} else {
	include "view/404.php";
}

?>
