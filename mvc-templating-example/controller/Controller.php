<?php

namespace Controller;

use Model\View;

abstract class Controller {

	//Force the creation of an index function.
	public abstract function index();

	protected function view($view_name) {
		return new View("view/{$view_name}.tpl");
	}
	
}

?>