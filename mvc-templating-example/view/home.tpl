<html>
    <head>
        <title>Home</title>
    </head>

    <body>
        {foreach from=$posts item=post}
            <div>
                <h2>
                    {* This is how you use variables from the Controller. *}
                    {$post->get('title')}
                </h2>
                <p>
                    {$post->get('content')}
                </p>
                <p>
                    Created on:
                    {* This is how you use variables from the Controller. *}
                    {$post->get('created')}
                </p>
            </div>
        {foreachelse}
            Er zijn geen berichten gevonden.
        {/foreach}
    </body>
</html>
