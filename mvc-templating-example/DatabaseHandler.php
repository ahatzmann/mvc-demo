<?php

class DatabaseHandler {
	private static $connection;

	public static function execute_statement($statement) {
		
		// Open database connection
		DatabaseHandler::open_database();
		
		// Execute database query
		$result = mysql_query($statement, DatabaseHandler::$connection) 
			or die(mysql_error());
		
		// Close database connection
		DatabaseHandler::close_database();
		
		// Return results
		return $result;
	}

	public static function execute_non_query($dml) {

		// Execute database query
		$result = DatabaseHandler::execute_statement($dml);

		// Get affected rows
		return mysql_affected_rows($result);
	}

	public static function execute_query($sql) {
		
		// Initialize dataset
		$dataset = FALSE;
		
		// Execute database query
		$result = DatabaseHandler::execute_statement($sql);
		
		// Count number of rows
		$count = mysql_num_rows($result);

		// Check for single row returned
		if ($count == 1) {
		
			// Fetch a single row from database cursor
			$dataset = mysql_fetch_assoc($result);	
		}
		
		// Check for multiple rows returned
		if ($count > 1) {

			// Initialize rows array
			$dataset = array();
			
			// Fetch all rows from database cursor
			while ($row = mysql_fetch_assoc($result)) {
				$dataset[] = $row;
			}
		}	
		
		// Return dataset
		return $dataset;
	}

	public static function open_database() {

		// Open database connection
		DatabaseHandler::$connection = mysql_connect("localhost", "root", "") 
			or die(mysql_error());
		
		// Select target database
		mysql_select_db("mvc_workshop", DatabaseHandler::$connection) 
			or die(mysql_error());
	}

	public static function close_database() {
		
		// Close database connection
		if (isset(DatabaseHandler::$connection)) {
			mysql_close(DatabaseHandler::$connection);
		}
	}

}

?>