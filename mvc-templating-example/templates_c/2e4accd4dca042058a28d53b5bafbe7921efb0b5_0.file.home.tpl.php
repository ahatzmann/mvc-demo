<?php
/* Smarty version 3.1.29, created on 2016-05-24 13:27:04
  from "D:\code\php\mvc-demo\mvc-templating-example\view\home.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57443a88001211_17988170',
  'file_dependency' => 
  array (
    '2e4accd4dca042058a28d53b5bafbe7921efb0b5' => 
    array (
      0 => 'D:\\code\\php\\mvc-demo\\mvc-templating-example\\view\\home.tpl',
      1 => 1464089222,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57443a88001211_17988170 ($_smarty_tpl) {
?>
<html>
    <head>
        <title>Home</title>
    </head>

    <body>
        <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_0_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_0_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
            <div>
                <h2>
                    
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->get('title');?>

                </h2>
                <p>
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->get('content');?>

                </p>
                <p>
                    Created on:
                    
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->get('created');?>

                </p>
            </div>
        <?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['post']->_loop) {
?>
            Er zijn geen berichten gevonden.
        <?php
}
if ($__foreach_post_0_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_item;
}
?>
    </body>
</html>
<?php }
}
